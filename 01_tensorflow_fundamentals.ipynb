{
 "cells": [
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "# Getting started with TensorFlow: A guide to the fundamentals\n",
    "\n",
    "## What is TensorFlow?\n",
    "\n",
    "[TensorFlow](https://www.tensorflow.org/) is an open-source end-to-end machine learning library for preprocessing data, modelling data and serving models (getting them into the hands of others).\n",
    "\n",
    "## Why use TensorFlow?\n",
    "\n",
    "Rather than building machine learning and deep learning models from scratch, it's more likely you'll use a library such as TensorFlow. This is because it contains many of the most common machine learning functions you'll want to use.\n",
    "\n",
    "## What we're going to cover\n",
    "\n",
    "TensorFlow is vast. But the main premise is simple: turn data into numbers (tensors) and build machine learning algorithms to find patterns in them.\n",
    "\n",
    "In this notebook we cover some of the most fundamental TensorFlow operations, more specificially:\n",
    "* Introduction to tensors (creating tensors)\n",
    "* Getting information from tensors (tensor attributes)\n",
    "* Manipulating tensors (tensor operations)\n",
    "* Tensors and NumPy\n",
    "* Using @tf.function (a way to speed up your regular Python functions)\n",
    "* Using GPUs with TensorFlow\n",
    "* Exercises to try\n",
    "\n",
    "Things to note:\n",
    "* Many of the conventions here will happen automatically behind the scenes (when you build a model) but it's worth knowing so if you see any of these things, you know what's happening.\n",
    "* For any TensorFlow function you see, it's important to be able to check it out in the documentation, for example, going to the Python API docs for all functions and searching for what you need: https://www.tensorflow.org/api_docs/python/ (don't worry if this seems overwhelming at first, with enough practice, you'll get used to navigating the documentaiton)."
   ],
   "id": "f7bb94c7e09d3633"
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "## Introduction to Tensors\n",
    "\n",
    "If you've ever used NumPy, [tensors](https://www.tensorflow.org/guide/tensor) are kind of like NumPy arrays (we'll see more on this later).\n",
    "\n",
    "For the sake of this notebook and going forward, you can think of a tensor as a multi-dimensional numerical representation (also referred to as n-dimensional, where n can be any number) of something. Where something can be almost anything you can imagine: \n",
    "* It could be numbers themselves (using tensors to represent the price of houses). \n",
    "* It could be an image (using tensors to represent the pixels of an image).\n",
    "* It could be text (using tensors to represent words).\n",
    "* Or it could be some other form of information (or data) you want to represent with numbers.\n",
    "\n",
    "The main difference between tensors and NumPy arrays (also an n-dimensional array of numbers) is that tensors can be used on [GPUs (graphical processing units)](https://blogs.nvidia.com/blog/2009/12/16/whats-the-difference-between-a-cpu-and-a-gpu/) and [TPUs (tensor processing units)](https://en.wikipedia.org/wiki/Tensor_processing_unit). \n",
    "\n",
    "The benefit of being able to run on GPUs and TPUs is faster computation, this means, if we wanted to find patterns in the numerical representations of our data, we can generally find them faster using GPUs and TPUs.\n",
    "\n",
    "Okay, we've been talking enough about tensors, let's see them.\n",
    "\n",
    "The first thing we'll do is import TensorFlow under the common alias `tf`"
   ],
   "id": "1312018bea230b9f"
  },
  {
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-04-08T17:07:50.998489Z",
     "start_time": "2024-04-08T17:07:39.109300Z"
    }
   },
   "cell_type": "code",
   "source": [
    "import tensorflow as tf\n",
    "tf.__version__"
   ],
   "id": "99d431151f7292f5",
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'2.13.0'"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "execution_count": 1
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "",
   "id": "fd08e9743146c81b"
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "### A. Creating Tensors with `tf.constant()`\n",
    "\n",
    "As mentioned before, in general, you usually won't create tensors yourself. This is because TensorFlow has modules built-in (such as [`tf.io`](https://www.tensorflow.org/api_docs/python/tf/io) and [`tf.data`](https://www.tensorflow.org/guide/data)) which are able to read your data sources and automatically convert them to tensors and then later on, neural network models will process these for us.\n",
    "\n",
    "But for now, because we're getting familar with tensors themselves and how to manipulate them, we'll see how we can create them ourselves.\n",
    "\n",
    "We'll begin by using [`tf.constant()`](https://www.tensorflow.org/api_docs/python/tf/constant)\n",
    "\n",
    "**First we should now what is Scalar, Vector, Matrix and Tensor**\n",
    "1. **Scalar**\n",
    "    * A scalar is a single numerical value, representing a magnitude without any direction.\n",
    "    * Examples: 5, -2.3, π.\n",
    "    * Scalars are zero-dimensional, meaning they have no inherent direction and are represented by a single number.\n",
    "2. **Vector**\n",
    "    * A vector is an ordered collection of scalar values, arranged in a single dimension.\n",
    "    * Each element in a vector represents a magnitude and a direction, typically in a specific coordinate system.\n",
    "    * Examples: [1, 2, 3], [0.5, -1.2, 3.7].\n",
    "    * Vectors are one-dimensional, meaning they have magnitude and direction along a single axis.\n",
    "3. Matrix\n",
    "    * A matrix is a two-dimensional array of scalar values arranged in rows and columns.\n",
    "    * Each element in a matrix is identified by its row and column indices.\n",
    "    * Examples:\n",
    "      ```\n",
    "        [1, 2, 3]\n",
    "        [4, 5, 6]\n",
    "      ```\n",
    "    * Matrices are commonly used to represent transformations, linear equations, and various mathematical operations.\n",
    "4. Tensor\n",
    "    * A tensor is a multi-dimensional array of scalar values.\n",
    "    * Tensors generalize scalars, vectors, and matrices to higher dimensions.\n",
    "    * Examples:\n",
    "      ```\n",
    "        [[[1, 2], [3, 4]], \n",
    "        [[5, 6], [7, 8]]]      \n",
    "      ```\n",
    "    * Tensors can have arbitrary shapes and sizes, including scalars, vectors, and matrices as special cases.\n",
    "    * In machine learning, tensors are fundamental data structures used to represent input data, model parameters, and output predictions.\n",
    "\n",
    "![difference between scalar, vector, matrix, tensor](https://raw.githubusercontent.com/mrdbourke/tensorflow-deep-learning/main/images/00-scalar-vector-matrix-tensor.png\n",
    "\n",
    "In summary, scalars represent single numerical values, vectors represent ordered collections of values in one dimension, matrices represent two-dimensional arrays, and tensors generalize to multi-dimensional arrays with arbitrary shapes. Each of these mathematical objects has its own properties and applications in various fields, including mathematics, physics, and machine learning."
   ],
   "id": "35f2ec5fad4ce67c"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "# create a scalar (no dimension \n",
    "a_scalar = tf.constant(1)\n",
    "\n",
    "# create a vector\n",
    "a_vector = tf.constant([1, 2, 3])\n",
    "\n",
    "# create a matrix\n",
    "a_matrix = tf.constant([[1, 2, 3], [4, 5, 6]])\n",
    "\n",
    "# create a tensor\n",
    "a_tensor = tf.constant([[[1, 2, 3], [4, 5, 6]]])\n",
    "\n",
    "print(f\"a_scalar:\\n {a_scalar}, shape: {a_scalar.shape}, dimensions: {a_scalar.ndim}\")\n",
    "print(f\"a_vector:\\n {a_vector}, shape: {a_vector.shape}, dimensions: {a_vector.ndim}\")\n",
    "print(f\"a_matrix:\\n {a_matrix}, shape: {a_matrix.shape}, dimensions: {a_matrix.ndim}\")\n",
    "print(f\"a_tensor:\\n {a_tensor}, shape: {a_tensor.shape}, dimensions: {a_tensor.ndim}\")\n"
   ],
   "id": "6a7fc447e5830053",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "By default, TensorFlow creates tensors with either an `int32` or `float32` datatype.\n",
    "\n",
    "This is known as [32-bit precision](https://en.wikipedia.org/wiki/Precision_(computer_science) (the higher the number, the more precise the number, the more space it takes up on your computer)."
   ],
   "id": "4c347ce0bfaa688"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "# Create another matrix and define the datatype\n",
    "ab_tensor = tf.constant([[10., 7.],\n",
    "                          [3., 2.],\n",
    "                          [8., 9.]], dtype=tf.float16) # specify the datatype with 'dtype'\n",
    "\n",
    "ab_tensor"
   ],
   "id": "989b05ad3500b870",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "### B. Creating Tensors with `tf.variable()`\n",
    "\n",
    "> tf.constant and tf.Variable are both ways to create tensors in TensorFlow, but they differ in mutability and usage. Tensors created with tf.constant are immutable, meaning their values cannot be changed after creation, while tensors created with tf.Variable are mutable and can be modified using methods like assign. tf.constant tensors are typically used for values that remain constant throughout the execution of the program, such as hyperparameters, while tf.Variable tensors are commonly used for model parameters that need to be updated during training, such as weights and biases in neural networks. Additionally, tf.Variable tensors can be optimized by TensorFlow's optimizers during training, while tf.constant tensors cannot be optimized as their values remain fixed.\n"
   ],
   "id": "af8210a2e76a6bbc"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "# create a scalar (no dimension \n",
    "b_scalar = tf.Variable(1)\n",
    "\n",
    "# create a vector\n",
    "b_vector = tf.Variable([1, 2, 3])\n",
    "\n",
    "# create a matrix\n",
    "b_matrix = tf.Variable([[1, 2, 3], [4, 5, 6]])\n",
    "\n",
    "# create a tensor\n",
    "b_tensor = tf.Variable([[[1, 2, 3], [4, 5, 6]]])\n",
    "\n",
    "print(f\"a_scalar:\\n {b_scalar}, shape: {b_scalar.shape}\")\n",
    "print(f\"a_vector:\\n {b_vector}, shape: {b_vector.shape}\")\n",
    "print(f\"a_matrix:\\n {b_matrix}, shape: {b_matrix.shape}\")\n",
    "print(f\"a_tensor:\\n {b_tensor}, shape: {b_tensor.shape}\")\n"
   ],
   "id": "ee14387f00f78a03",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "Which one should you use? `tf.constant()` or `tf.Variable()`?\n",
    "\n",
    "It will depend on what your problem requires. However, most of the time, TensorFlow will automatically choose for you (when loading data or modelling data)"
   ],
   "id": "18faeee9b494b015"
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "### C. Creating random tensors\n",
    "https://www.tensorflow.org/api_docs/python/tf/random\n",
    "\n",
    "Random tensors are tensors of some abitrary size which contain random numbers.\n",
    "\n",
    "Why would you want to create random tensors? \n",
    "\n",
    "This is what neural networks use to intialize their weights (patterns) that they're trying to learn in the data.\n",
    "\n",
    "For example, the process of a neural network learning often involves taking a random n-dimensional array of numbers and refining them until they represent some kind of pattern (a compressed way to represent the original data).\n",
    "\n",
    "**How a network learns**\n",
    "![how a network learns](https://raw.githubusercontent.com/mrdbourke/tensorflow-deep-learning/main/images/00-how-a-network-learns.png)\n",
    "*A network learns by starting with random patterns (1) then going through demonstrative examples of data (2) whilst trying to update its random patterns to represent the examples (3).*\n",
    "\n",
    "We can create random tensors by using the [`tf.random.Generator`](https://www.tensorflow.org/guide/random_numbers#the_tfrandomgenerator_class) class"
   ],
   "id": "2efb77bbbc2db95c"
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "In TensorFlow, `tf.random.set_seed()` is a function used to set the seed for the global random number generators. Setting the seed ensures that the sequence of random numbers generated by TensorFlow operations will be reproducible across different runs of the program. This is particularly useful when you want to ensure that your results are consistent and reproducible, especially in scenarios like model training and experimentation, where randomness plays a role (e.g., initializing model weights randomly, shuffling datasets).\n",
    "\n"
   ],
   "id": "b189de89800574b3"
  },
  {
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-04-08T18:23:15.874263Z",
     "start_time": "2024-04-08T18:23:15.781857Z"
    }
   },
   "cell_type": "code",
   "source": "c_random_seed = tf.random.Generator.from_seed(42) # set the seed for reproducibility",
   "id": "d3e4116f94567e60",
   "outputs": [],
   "execution_count": 21
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "There are several ways to create random tensors in TensorFlow, depending on your specific requirements. Here are some common methods:\n",
    "* `tf.random.normal`: This function generates random values from a normal (Gaussian) distribution with specified mean and standard deviation.\n",
    "* `tf.random.uniform`: This function generates random values from a uniform distribution within a specified range.\n",
    "* `tf.random.truncated_normal`: Similar to tf.random.normal, but the values more than two standard deviations from the mean are discarded and re-drawn.\n",
    "* `tf.random.shuffle`: This function shuffles the elements of a tensor along the first dimension."
   ],
   "id": "efb83984c69099ec"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "outputs": [],
   "execution_count": null,
   "source": "",
   "id": "707ac1c9ed421b8f"
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
